import { Component } from '@angular/core';
import { Auth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  constructor(
    private auth: Auth,
    private authService: AuthService,
    private router: Router
  ) {}

  loginViaGoogle() {
    this.authService
      .loginUsingGoogle()
      .then((result) => {
        this.router.navigateByUrl('/');
      })
      .catch((error) => {
        console.log(`errorCode = ${error.code}`);
        console.log(`errorMessage = ${error.message}`);
      });
  }
}
