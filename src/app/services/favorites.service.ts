import { Injectable } from '@angular/core';
import { collection, collectionData, Firestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root',
})
export class FavoritesService {
  constructor(private firestore: Firestore) {}

  getFavorites() {
    const favoritesCol = collection(this.firestore, 'Favorites');
    return collectionData(favoritesCol);
  }
}
