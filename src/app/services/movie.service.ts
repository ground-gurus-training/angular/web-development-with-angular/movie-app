import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class MovieService {
  baseUrl = 'https://api.themoviedb.org/3';
  apiKey = 'd2d6b3b39e1a0d894143508f7ddce644';

  constructor(private http: HttpClient) {}

  getMovie() {
    return this.http.get(`${this.baseUrl}/movie/550?api_key=${this.apiKey}`);
  }

  search(query: string) {
    return this.http.get(
      `${this.baseUrl}/search/movie?api_key=${this.apiKey}&query=${query}`
    );
  }
}
