import { Component, OnInit } from '@angular/core';
import { Auth, onAuthStateChanged } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Movie } from '../model/movie';
import { UserDetails } from '../model/user';
import { AuthService } from '../services/auth.service';
import { MovieService } from '../services/movie.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  user$: BehaviorSubject<UserDetails>;
  isLoggedIn = false;
  movies: Movie[] = [];
  query = '';

  constructor(
    private auth: Auth,
    private movieService: MovieService,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit() {
    onAuthStateChanged(this.auth, (user) => {
      if (user) {
        this.user$ = new BehaviorSubject({
          displayName: user.displayName,
          email: user.email,
        });
        this.isLoggedIn = true;
      } else {
        console.log('User is logged out');
      }
    });

    this.movieService.getMovie().subscribe((data: any) => {
      this.movies.push({
        title: data.title,
        homePage: data.homepage,
        posterUrl: `https://image.tmdb.org/t/p/w200${data.poster_path}`,
        overview: data.overview,
      });
    });
  }

  search() {
    if (!this.query) return;

    this.movies = [];
    this.movieService.search(this.query).subscribe((data: any) => {
      const results = data.results;
      results.forEach((result) => {
        const posterUrl = result.poster_path
          ? `https://image.tmdb.org/t/p/w200${result.poster_path}`
          : '';
        this.movies.push({
          title: result.title,
          homePage: result.homepage,
          posterUrl,
          overview: result.overview,
        });
      });
    });
  }

  clear() {
    this.query = '';
    this.movies = [];
  }

  async logout() {
    await this.authService.logout();
    window.location.reload();
  }

  goToLogin() {
    this.router.navigateByUrl('/login');
  }
}
