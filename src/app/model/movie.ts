export interface Movie {
  title: string;
  homePage: string;
  posterUrl: string;
  overview: string;
}
