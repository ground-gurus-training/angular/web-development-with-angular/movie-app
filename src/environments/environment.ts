// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBEPqq6rej1K42YJpX-h-O8H-30IFdLnW0',
    authDomain: 'movie-app-56d19.firebaseapp.com',
    projectId: 'movie-app-56d19',
    storageBucket: 'movie-app-56d19.appspot.com',
    messagingSenderId: '1067228989170',
    appId: '1:1067228989170:web:8cffcf32b44276555df26b',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
